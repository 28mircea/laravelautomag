<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('search');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/users', [AdminController::class, 'index'])->middleware('admin');

Route::get('/changePassword', [AdminController::class, 'changePasswordForm'])->name('changePassword');
Route::post('/changePassword',[AdminController::class, 'changePassword'])->name('changePassword');

Route::get('/editUser', [AdminController::class, 'editUserForm'])->name('editUser');
Route::post('/updateUser', [AdminController::class, 'updateUser'])->name('updateUser');

Route::delete('/deleteUser/{user}',[AdminController::class, 'deleteUser'])->middleware('admin');

Route::get('/mycars', [CarController::class, 'mycars'])->name('mycars')->middleware('user');

Route::get('/destroypicture/{image}',[CarController::class, 'destroyPicture'])->name('destroypicture')->middleware('user');
Route::get('/search',[CarController::class, 'process'])->name('search');

Route::get('cart', [CarController::class, 'cart']);
Route::get('add-to-cart/{car}', [CarController::class ,'addToCart']);
Route::patch('update-cart', [CarController::class, 'updateCart']); 
Route::delete('remove-from-cart', [CarController::class, 'remove']);

Route::resource('cars', CarController::class);
