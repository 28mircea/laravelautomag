@extends('layouts.app')
@section('content')
<div class="container table-responsive">
    <h1>Add Car</h1>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>	    	
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>    
    @endif
    <form method="POST" action="{{route('cars.store')}}" class="text-center border border-light p-5" enctype="multipart/form-data">
        @csrf
        <table class="table table-bordered">
            <tr>
               <td><label for="brand">Car Brand</label></td>
               <td><input type="text" name="brand" value="{{old('price')}}"></td>     
            </tr>
            <tr>
            <td><label for="year">Car Year</label></td>
                <td>
                    <select name="year" id="year" value="{{old('year')}}" >
                        <option value="{{old('year')}}" selected="selected">{{old('year')}}</option>                        
                                
                                @for ($year=date('Y'); $year>=1960; $year--)                                
                                    <option value="{{ $year }}">{{ $year }}</option>
                                @endfor   
                            
                    </select>    
                </td>          
            </tr>
            <tr>
                <td><label for="price">Car Price</label></td>
                <td><input type="number" name="price" value="{{old('price')}}"></td>
            </tr>
            <tr>
                <td><label for="gearbox">Car Gearbox</label></td>
                <td>    
                    <input type="radio" name="gearbox" id="gearbox" class ="mr-1" value='Manual'@if (old('gearbox') == 'Manual') checked @endif >Manual
                    <input type="radio" name="gearbox" id="gearbox" class ="mr-1" value='Automatic'@if (old('gearbox') == 'Automatic') checked @endif >Automatic
                </td>            
            </tr>
            <tr>
                <td><label for="emissions" class="col-md-4 col-form-label">Emissions Class</label></td>
                <td>
                    <select name="emissions" id="emissions">
                    <option value="{{old('emissions')}}" selected="selected">{{old('emissions')}}</option>
                                            <option value="Euro1">Euro1</option>
                                            <option value="Euro2">Euro2</option>
                                            <option value="Euro3">Euro3</option>
                                            <option value="Euro4">Euro4</option>
                                            <option value="Euro5">Euro5</option>
                                            <option value="Euro6">Euro6</option>
                    </select>
                </td>          
            </tr>
            <tr>
                <td><label for="service" class="col-form-label">Service Manual</label></td>
                <td><input type="checkbox" name="service" id="service" value='service' @if (old('service') == 'service') checked @endif></td>
            </tr>
            <tr>
                <td><label for="information">Other Information</label></td>
                <td><textarea name="information" id="information" rows="5" class="form-control" placeholder="(other information)"></textarea></td>
            </tr>
            <tr>
                <td><label for="images">Select Images</label></td> 
                <td><input type="file" name="images[]" id="images" multiple="multiple"></td>
                           
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" name="user_id" value="{{Auth::id()}}">
                    <input type="submit" class="btn btn-primary" value="Add Car">
                </td>    
            </tr>            
        </table>    
    </form>
</div>        
@endsection