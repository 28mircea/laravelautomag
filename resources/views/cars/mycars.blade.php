@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Show MyCars</h1>
    <a href="{{route('cars.create')}}" class="btn btn-primary">Create Car</a>
    <br><br>        
        <table class="table table-stripped table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th class="text-center">Car Id</th>
                <th class="text-center">Car Brand</th>
                <th class="text-center">Car Price</th>
                <th class="text-center">Car Pictures</th>
                <th class="text-center">User Email</th>
                <th class="text-center">Actions</th>
            </tr>         
            </thead>
            <tbody>
                @foreach($cars as $car)
                <tr>
                    <td class="text-center">{{ $car->id }}</td>
                    <td class="text-center">{{ $car->brand }}</td>
                    <td class="text-center">{{ $car->price }}</td>
                    <td align="center">
                        <div id="carouselExampleControls" class="carousel slide" data-interval="false" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                @foreach( $car->carphotos as $carphoto )    
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        <img class="d-block img-fluid" src="{{ asset('storage/images/'.$carphoto->image) }}" width="200">             
                                    </div>
                                @endforeach                                
                            </div>
                        </div>
                    </td>
                    <td class="text-center">{{ $car->user->email }}</td>
                    <td class="text-center">
                        <a href="{{ route('cars.show', $car->id) }}" class="btn btn-info">Show</a>
                        <br><br>
                        <a href="{{ route('cars.edit', $car->id) }}" class="btn btn-warning">Edit</a>
                        <br><br>
                        <form method="POST" action="{{ route('cars.destroy', $car->id) }}">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>       
                    </td>
                </tr>
                @endforeach
            </tbody>        
        </table>
        <div class="pagination-block">
            {{ $cars->appends(request()->input())->links('layouts.paginationlinks') }}
        </div>   
</div>           
@endsection