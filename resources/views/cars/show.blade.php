@extends('layouts.app')
@section('content')
<div class="container">
<h1 align="left">Show Car</h1>    
<br><br>    
        <table class="table table-bordered">
            <tr>
                <td colspan="8" align="center">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach( $car->carphotos as $carphoto )
                                <li data-target="#carouselExampleCaptions" data-slide-to="{{$loop->index}}" class="{{ $loop->first ? 'active' : '' }}"></li>
                            @endforeach
                        </ol> 
                        <div class="carousel-inner">
                            <div class="carousel-inner" role="listbox">
                                @foreach( $car->carphotos as $carphoto )    
                                    <div class="carousel-item {{ $loop->iteration == 1 ? 'active' : '' }}">
                                        <img class="d-block img-fluid" src="{{asset('storage/images/'.$carphoto->image)}}" width="600">             
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div> 
                </td>        
            </tr>            
            <tr>    
                <th class="text-center">Car Brand</th>
                <td class="text-center" >{{ $car->brand }}</td>
            </tr>
            <tr>    
                <th class="text-center">Car Year</th>
                <td class="text-center">{{ $car->year }}</td>
            </tr>
            <tr>    
                <th class="text-center">Car Price</th>
                <td class="text-center">{{ $car->price }}</td>
            </tr>
            <tr>    
                <th class="text-center">Car Gearbox</th>
                 <td class="text-center">{{ $car->gearbox }}</td>
            </tr>
            <tr>     
                <th class="text-center" >Car Emissions</th>
                 <td class="text-center" >{{ $car->emissions }}</td>
            </tr>
            <tr>             
                <th class="text-center" >Car Service</th>
                <td class="text-center">{{ $car->service }}</td>
            </tr>
            <tr>            
                <th class="text-center">Car Information</th>
                <td class="text-center">{{ $car->information }}</td>                      
            </tr>
        </table>
</div>
@endsection