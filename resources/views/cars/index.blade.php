@extends('layouts.app')
@section('content')
<div class="container table-reponsive">
    <h1>Show Cars</h1>    
    <table class="table">
        <thead>
           <tr>
               <th class="text-center">Car Brand</th>
               <th class="text-center">Car Price</th>
               <th class="text-center">Car Images</th>
               <th class="text-center">User Info</th>
               <th class="text-center">Actions</th>
          </tr>         
        </thead>
        <tbody>
            @foreach($cars as $car)
                <tr>
                    <td class="text-center">{{ $car->brand }}</td>
                    <td class="text-center">{{ $car->price }}</td>
                    <td align="center">
                        <div id="carouselExampleControls" class="carousel slide" data-interval="false" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                @foreach( $car->carphotos as $carphoto )    
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        <img class="d-block img-fluid" src="{{ asset('storage/images/'.$carphoto->image) }}" width="200">             
                                    </div>
                                @endforeach                                
                            </div>
                        </div>
                    </td>
                    <td class="text-center">{{ $car->user->email }}</td>
                    <td class="text-center">                        
                        <form method="POST" action="{{ route('cars.destroy', $car->id) }}">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>    
                    </td>
                </tr>
            @endforeach
        </tbody>        
    </table>
</div>           
@endsection