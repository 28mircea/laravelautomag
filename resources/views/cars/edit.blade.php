@extends('layouts.app')
@section('content')
<div class="container table-responsive">
    <h1>Edit Car</h1>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>	    	
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>    
    @endif
    <form method="POST" action="{{route('cars.update',$car->id)}}" class="text-center border border-light p-5" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <table class="table">
            <tr>
               <td><label for="brand">Car Brand</label></td>
               <td><input type="text" name="brand" value="{{ $car->brand }}"></td>     
            </tr>
            <tr>
                <td><label for="year">Car Year</label></td>
                <td>
                    <select name="year" id="year" value="{{old('year')}}" >
                        <option value="{{ $car->year }}" selected="selected">{{ $car->year }}</option>                        
                                
                                @for ($year=date('Y'); $year>=1960; $year--)                                
                                    <option value="{{ $year }}">{{ $year }}</option>
                                @endfor   
                            
                    </select>    
                </td>          
            </tr>
            <tr>
                <td><label for="price">Car Price</label></td>
                <td><input type="number" name="price" value="{{ $car->price }}"></td>
            </tr>
            <tr>
                <td><label for="gearbox">Car Gearbox</label></td>
                <td>    
                    <input type="radio" name="gearbox" id="gearbox" class ="mr-1" value='Manual'@if ($car->gearbox == 'Manual') checked @endif >Manual
                    <input type="radio" name="gearbox" id="gearbox" class ="mr-1" value='Automatic'@if ($car->gearbox == 'Automatic') checked @endif >Automatic
                </td>            
            </tr>
            <tr>
                <td><label for="emissions" class="col-md-4 col-form-label">Emissions Class</label></td>
                <td>
                    <select name="emissions" id="emissions">
                    <option value="{{ $car->emissions }}" selected="selected">{{ $car->emissions }}</option>
                                            <option value="Euro1">Euro1</option>
                                            <option value="Euro2">Euro2</option>
                                            <option value="Euro3">Euro3</option>
                                            <option value="Euro4">Euro4</option>
                                            <option value="Euro5">Euro5</option>
                                            <option value="Euro6">Euro6</option>
                    </select>
                </td>          
            </tr>
            <tr>
                <td><label for="service" class="col-form-label">Service Manual</label></td>
                <td><input type="checkbox" name="service" id="service" value='service' @if ($car->service == 'service') checked @endif></td>
            </tr>
            <tr>
                <td><label for="information">Other Information</label></td>
                <td><textarea name="information" id="information" rows="5" class="form-control">{{ $car->information }}</textarea></td>
            </tr>
            <tr>                
                @foreach( $car->carphotos as $carphoto )
                    <td>            
                        <img src="{{asset('storage/images/'.$carphoto->image)}}" width="100">             
                        <br><br>
                        <a href="/destroypicture/{{ $carphoto->id }}" class="btn btn-danger" onclick="return confirm('Are you sure to delete this picture?');">Delete</a>
                    </td>
                @endforeach            
            </tr>
            <tr>
                <td><label for="images">Select Images</label></td>
                <td><input type="file" name="images[]" id="images" multiple="multiple"></td>                            
            </tr>
            <tr>
                <td colspan="2">                    
                   <input type="submit" class="btn btn-primary" value="Update Car">
                </td>    
            </tr>            
        </table>    
    </form>
</div>        
@endsection