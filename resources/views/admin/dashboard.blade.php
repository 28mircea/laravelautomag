@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Users Management</h2>
	        </div>	        
	    </div>
	</div>	
	<table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center" >User List</th>
                <th colspan="3" class="text-center">User Policies</th>               
            </tr>
            <tr rowspan="2">    	
                <th class="text-center">Id</th>
                <th class="text-center" >Name</th>
                <th class="text-center">Email</th>
                <th class="text-center">Role</th>
                <th class="text-center">Actions</th>                     
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td class="text-center">{{ $user->id }}</td>
                    <td class="text-center">{{ $user->name }}</td>
                    <td class="text-center">{{ $user->email }}</td>
                    <td class="text-center">{{ $user->role }}</td>                
                    <td class="text-center">
                        <form action="/deleteUser/{{ $user->id }}" method="post">
                            @csrf 
                            @method('delete')
                            <input type="submit" value="Delete" class="btn btn-primary">
                        </form> 
                    </td>               
                </tr>
            @endforeach
        </tbody>
	</table>
    <br><br>        
    <h1>Users Car</h1>                                       
    <table class="table">
        <thead>
        <tr>
            <th class="text-center">Car Brand</th>
            <th class="text-center">Car Price</th>
            <th class="text-center">Car Images</th>
            <th class="text-center">User Id</th>                   
            <th class="text-center">Actions</th>
        </tr>         
        </thead>
        <tbody>
            @foreach($cars as $car)
                <tr>
                    <td class="text-center">{{ $car->brand }}</td>
                    <td class="text-center">{{ $car->price }}</td>
                    <td align="center">
                        <div id="carouselExampleControls" class="carousel slide" data-interval="false" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                @foreach( $car->carphotos as $carphoto )    
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        <img class="d-block img-fluid" src="{{ asset('storage/images/'.$carphoto->image) }}" width="200">             
                                    </div>
                                @endforeach                                
                            </div>
                        </div>
                    </td>
                    <td class="text-center">{{ $car->user_id }}</td>                           
                    <td class="text-center">                        
                        <a href="{{ route('cars.show', $car->id) }}" class="btn btn-info">Show</a>
                        <br><br>  
                    </td>
                </tr>
            @endforeach
        </tbody>        
    </table> 
</div>    	
@endsection
