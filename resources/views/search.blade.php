@extends('welcome')
@section('content')
<div class="container table-responsive">
    <h1>AutoMag</h1>
    <form method="get" action="/search" class="text-center border border-light p-5">
        @csrf
        <table class="table">
            <tr>
               <td><label for="brand">Car Brand</label></td>
               <td><input type="text" name="brand" id="brand" placeholder="(introduce car brand)"></td>     
            </tr>
            <tr>
                <td><label for="year">Car Year</label></td>                
                <td>
                    <select name="startyear" id="startyear">
                        <option value="0" selected="selected">(from year)</option>
                                @for ($startyear=date('Y'); $startyear>=1960; $startyear--)                                
                                    <option value="{{ $startyear }}">{{ $startyear }}</option>
                                @endfor
                    </select>    
                </td>
                <td>
                    <select name="endyear" id="endyear">
                        <option value="0" selected="selected">(to year)</option>
                                @for ($endyear=date('Y'); $endyear>=1960; $endyear--)                                
                                    <option value="{{ $endyear }}">{{ $endyear }}</option>
                                @endfor
                    </select>    
                </td>
            </tr>
            <tr>
                <td><label for="startprice">Start Price</label></td>
                <td><input type="number" name="startprice" id="startprice" value="0"></td>
                <td><label for="endprice">End Price</label></td>
                <td><input type="number" name="endprice" id="endprice" value="99999"></td>
            </tr>
            <tr>
                <td><label for="gearbox">Select Gearbox</label></td>
                <td>    
                    <input type="radio" name="gearbox" class ="mr-1" value="Manual" @if (old('gearbox') == 'Manual')  checked @endif >Manual
                    <input type="radio" name="gearbox" class ="mr-1" value="Automatic" @if (old('gearbox') == 'Automatic')  checked @endif >Automatic
                </td>
            </tr>
            <tr>
                <td><label for="emissions" class="col-md-4 col-form-label text-md-right">Select Emissions</label></td>
                <td>
                    <select name="emissions" id="emissions">
                    <option value="0" selected="selected">(select emissions)</option>
                                            <option value="Euro1">Euro1</option>
                                            <option value="Euro2">Euro2</option>
                                            <option value="Euro3">Euro3</option>
                                            <option value="Euro4">Euro4</option>
                                            <option value="Euro5">Euro5</option>
                                            <option value="Euro6">Euro6</option>
                    </select>
                </td>    
            </tr>
            <tr>
                <td><label for="service" class="col-form-label">Service Book</label></td>
                <td><input type="checkbox" name="service" id="service" value='service'></td>
            </tr>
            <tr>
                <td><span>Filter Results:</span></td>
                <td><select name="fetchval" id="fetchval" class="form-control">
                    <option value="" disabled="" selected="">Select Filter</option>
                    <option value="asc">Price Ascending</option>
                    <option value="desc">Price Descending</option>
                    <option value="newest">Newest</option>
                    <option value="oldest">Oldest</option>
                </select>
                </td>
            </tr>    
            <tr>
                <td><button type="button" class="btn btn-warning" onclick="resetSearch()">Reset Search</button></td>
                <td><input type="submit" class="btn btn-primary" value="Search Car"></td>                    
            </tr>            
        </table>    
    </form> 
        @if(isset($cars))            
            <br>      
            <table class="table table-stripped table-bordered table-hover" id="search-cars">
                <thead class="thead-dark">
                <tr>
                    <th class="text-center">Car Brand</th>
                    <th class="text-center">Car Price</th>
                    <th class="text-center">Car Images</th>                    
                    <th class="text-center">Actions</th>
                </tr>         
                </thead>
                <tbody>
                @if(count($cars) > 0) 
                    @foreach($cars as $car)
                        <tr>
                            <td class="text-center">{{ $car->brand }}</td>
                            <td class="text-center">{{ $car->price }}</td>
                            <td align="center">
                                <div id="carouselExampleControls" class="carousel slide" data-interval="false" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        @foreach( $car->carphotos as $carphoto )    
                                            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                                <img class="d-block img-fluid" src="{{ asset('storage/images/'.$carphoto->image) }}" width="200">             
                                            </div>
                                        @endforeach                                
                                    </div>
                                </div>
                            </td>                           
                            <td class="text-center">                        
                                <a href="{{ route('cars.show', $car->id) }}" class="btn btn-info">Show</a>
                                <a href="{{ url('add-to-cart/'.$car->id) }}" class="btn btn-danger">Add to Cart</a>
                                <br><br>  
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="6"><h4 class="text-center">No cars found!</h4></td></tr>    
                @endif    
                </tbody>        
            </table>
            <div class="pagination-block">
                {{ $cars->appends(request()->input())->links('layouts.paginationlinks') }}
            </div>  
        @endif
</div>                              
@endsection