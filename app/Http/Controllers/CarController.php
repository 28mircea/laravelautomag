<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Car;
use App\Models\CarPhoto;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\UploadRequest;
use App\Http\Requests\UpdateRequest;

class CarController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth')->except(['process','show','cart','addToCart','updateCart','remove']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::with('user')->get();
        return view('cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cars.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadRequest $request)
    {
        $car = Car::create($request->all());
        //$car = new Car();
        //$car->brand = $request->brand;
        //$car->price =$request->price;
        //$car->user_id = \Auth::id();
        $car->save();

        if($request->hasFile('images')){
            $this->storeimages($car, $request->images);
        }
        
        return redirect('/mycars');
    }

    protected function storeimages($car, $images)
    {       
        foreach($images as $image){
            $ext = $image->getClientOriginalExtension();
            $filename = date('YmdHis').rand(1,99999).'.'.$ext;            
            $filepath = $image->storeAs('public/images',$filename);

            CarPhoto::create([
                'car_id' => $car->id,
                'image' => $filename
            ]);
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return view('cars.show',  compact('car'));
    }

    public function mycars(){
        $cars = Car::where('user_id', auth()->user()->id)->paginate(2);
        return view('cars.mycars', compact('cars'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        return view('cars.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroyPicture($id)
    {        
        $carphoto = CarPhoto::find($id);        
        Storage::delete('public/images/'.$carphoto->image);       
        $carphoto->delete();        
        return \Redirect::back()->with('carphoto', $carphoto);
    } 


    public function update(UpdateRequest $request, Car $car)
    {
        $car->brand = $request->brand;
        $car->year = $request->year;
        $car->price = $request->price;
        $car->gearbox = $request->gearbox;
        $car->emissions = $request->emissions;
        $car->service = $request->service;
        $car->information = $request->information;
        $car->save();

        if($request->hasFile('images')){
            $this->storeimages($car, $request->images);
        }
        
        return redirect('/mycars')->withMessage ('Success','Car has been updated!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        foreach( $car->carphotos as $carphoto ){
            Storage::delete('public/images/'.$carphoto->image);
        }

        $car->delete();

        return redirect('/mycars')->withMessage ('Success','Car has been deleted!');
    }


    public function process(Request $request)
    {
        $brand = $request->query('brand', "");
        //dd($brand);
        $words = explode(" ", $brand);

        $startyear = $request->query('startyear');       
        $endyear = $request->query('endyear');  

        $startprice = $request->query('startprice',0);
        $endprice = $request->query('endprice',99999);

        $gearbox = $request->query('gearbox');
        $emissions = $request->query('emissions');
        $service = $request->query('service');
        
        $query = Car::where('id', '>', 0);

        foreach($words as $word){
            $query->where('brand','like',"%$word%");
        }

        if ($startyear && $endyear){
            $query->whereBetween('year', [$startyear, $endyear]);
        }

        if($startprice && $endprice){
            $query->whereBetween('price', [$startprice, $endprice]);
        }    
               
        if($gearbox){
            $query->where('gearbox','like',"%$gearbox%");
        }

        if($emissions){
            $query->where('emissions','like',"%$emissions%");
        }

        if($service){
            $query->where('service','like',"%$service%");
        }

                
        if('asc' == $request->query('fetchval')){
            $query->orderBy('price', 'ASC');
        }
        
        if('desc' == $request->query('fetchval')){
            $query->orderBy('price','DESC');
        }

        if('newest' == $request->query('fetchval')){
            $query->orderBy('created_at','DESC');
        }

        if('oldest' == $request->query('fetchval')){
            $query->orderBy('created_at','ASC');
        } 

        $cars = $query->paginate(2);    
        //dd($cars);
        return view('search', compact('cars'));
    }


    public function cart()
    {
        return view('cart');
    }


    public function addToCart($id)
    {
        $car = Car::find($id);
 
        if(!$car){ 
            abort(404); 
        }
 
        $cart = session()->get('cart');
 
        // if cart is empty then this the first product
        if(!$cart) {
 
            $cart = [
                    $id => [
                        "brand" => $car->brand,
                        "quantity" => 1,
                        "price" => $car->price                        
                    ]
            ];
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Car added to cart successfully!');
        }
 
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])){ 
            $cart[$id]['quantity']++; 
            session()->put('cart', $cart); 
            return redirect()->back()->with('success', 'Car added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "brand" => $car->brand,
            "quantity" => 1,
            "price" => $car->price            
        ];
 
        session()->put('cart', $cart);
 
        return redirect()->back()->with('success', 'Car added to cart successfully!');    
    }


    public function updateCart(Request $request)
    {
        if($request->id && $request->quantity){        
            $cart = session()->get('cart'); 
            $cart[$request->id]["quantity"] = $request->quantity; 
            session()->put('cart', $cart); 
            session()->flash('success', 'Cart updated successfully');
        }
    }
    

    public function remove(Request $request)
    {
        if($request->id){ 
            $cart = session()->get('cart'); 
            if(isset($cart[$request->id])){ 
                unset($cart[$request->id]); 
                session()->put('cart', $cart);
            } 
            session()->flash('success', 'Car removed successfully');
        }
    }
}
