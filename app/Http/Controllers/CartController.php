<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    
    public function cart()
    {
        return view('cart');
    }


    public function addToCart($id)
    {
        $car = Car::find($id);
 
        if(!$car){ 
            abort(404); 
        }
 
        $cart = session()->get('cart');
 
        // if cart is empty then this the first product
        if(!$cart) {
 
            $cart = [
                    $id => [
                        "brand" => $car->brand,
                        "quantity" => 1,
                        "price" => $car->price                        
                    ]
            ];
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Car added to cart successfully!');
        }
 
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])){ 
            $cart[$id]['quantity']++; 
            session()->put('cart', $cart); 
            return redirect()->back()->with('success', 'Car added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "brand" => $car->brand,
            "quantity" => 1,
            "price" => $car->price            
        ];
 
        session()->put('cart', $cart);
 
        return redirect()->back()->with('success', 'Car added to cart successfully!');    
    }


    public function updateCart(Request $request)
    {
        if($request->id && $request->quantity){        
            $cart = session()->get('cart'); 
            $cart[$request->id]["quantity"] = $request->quantity; 
            session()->put('cart', $cart); 
            session()->flash('success', 'Cart updated successfully');
        }
    }


    public function remove(Request $request)
    {
        if($request->id){ 
            $cart = session()->get('cart'); 
            if(isset($cart[$request->id])){ 
                unset($cart[$request->id]); 
                session()->put('cart', $cart);
            } 
            session()->flash('success', 'Car removed successfully');
        }
    }
    
}
