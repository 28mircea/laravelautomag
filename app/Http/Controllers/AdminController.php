<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Car;
use App\Models\CarPhoto;
use Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');        
    }

    public function index() 
    {
        $users = User::all();
        $cars = Car::all();
        return view('admin.dashboard', compact('users','cars'));
    }

    public function editUserForm()
    {        
        return view('admin.edit');
    }

    public function updateUser(Request $request)
    {        
        $this->validate(request(), [
            'name' => 'required',            
            'email' => 'required|email|max:255|unique:users'    
        ]);

        $user = Auth::user();
        $user->name = $request->get('name');        
        $user->email = $request->get('email');
        $user->save();
        return redirect('home')->with('success','User has been updated');
    }

    public function changePasswordForm()
    {
        return view('admin.changepassword');
    }

    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))){
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password                    
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        
        //return redirect()->back()->with("success","Password changed successfully !");
        return view('home')->with("success","Password changed successfully !");            
    }

    public function deleteUser(User $user)
    {
        $user->delete();
        return redirect('/users')->with('success','User has been deleted!');
    }

}
