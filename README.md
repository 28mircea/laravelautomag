# LaravelAutoMag

## How to use

- Clone the repository with git clone;
- Copy .env.example file to .env and edit database credentials there;
- Create the database in phpMyAdmin. Use the database name from  .env  file;
- Run php artisan migrate --seed (it has some seeded data);
- Run php artisan serve;
- Run npm run dev;
- That's it: launch the main URL;



## License
Basically, feel free to use and re-use any way you want.

